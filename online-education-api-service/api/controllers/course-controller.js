'use strict';


let faker = require('faker');



module.exports = {
  courses: getCourses,
  user: getUser,
  news: getNews,
  
};


function getCourses(req, res) {
  const course = ["Vue", "React", "Python", " Angular", "Java", "Java-Script", "C++", "C#","Ruby"]
  res.json([{

    "id": "" + faker.random.number(),
    "nameOfSubject": "Курс по   " + course[Math.floor(Math.random() * course.length)],
    "price": "цена : " + faker.random.number({
      min:50,
      max:100}) + "$",
    "nameOfTeacher":" " + faker.name.findName(),},
    
    {
      "id": "" + faker.random.number(),
    "nameOfSubject": "Курс по   " + course[Math.floor(Math.random() * course.length)],
    "price": "цена : " + faker.random.number({
      min:50,
      max:100}) + "$",
    "nameOfTeacher":" " + faker.name.findName(),},

    {
      "id": "" + faker.random.number(),
    "nameOfSubject": "Курс по   " + course[Math.floor(Math.random() * course.length)],
    "price": "цена : " + faker.random.number({
      min:50,
      max:100}) + "$",
    "nameOfTeacher":" " + faker.name.findName(),
  }]);
}

function getUser(req, res) {
  const sex = ["male", "feamale"]
  res.json([{
    "username": "" + faker.name.firstName() + " " + faker.name.lastName(),
    "email": "" + faker.internet.email(),
    "userpic": "images/" + faker.random.number({ min: 1, max: 4 }) + ".png",
    "sex": "" + sex[Math.floor(Math.random() * sex.length)],
    "age": "" + faker.random.number({
      min:16,
      max:80}) + "лет",
  }]);
}

function getNews(req, res) {
  res.json([{
    "nameOfArticles": "" + faker.lorem.words(2),
    "description": "" + faker.lorem.sentences(7),
    "image": "images2/" + faker.random.number({ min: 1, max: 4 }) + ".png",
  }]);
}

