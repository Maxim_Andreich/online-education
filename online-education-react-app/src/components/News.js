import React from 'react';
import * as Api from 'typescript-fetch-api'

import pic from './images2/1.png';

const api = new Api.DefaultApi()

class Nnews extends React.Component {

  constructor(props) {
    super(props);
    this.state = { news: [] };

    
  }


  fetchData = date => {api.news({ page: date }).then(response => this.setState({news: response}))}
  componentDidMount(){
    this.fetchData(this.props.date)
  
  }
  componentDidUpdate(prevProps){
    if(this.props.date!==prevProps.date) {
      this.fetchData(this.props.date)
    }

  }

  render() {
    return <div>
      
        {this.state.news.map((News) => <div>{News.nameOfArticles}</div>)}
        {this.state.news.map((News) => <div>{News.description}</div>)}
      
    </div>
  }
}

export default Nnews;