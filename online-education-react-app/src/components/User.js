import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class Uuser extends React.Component {

  constructor(props) {
    super(props);
    this.state = { user: [] };

    
  }


  fetchData = date => {api.user({ page: date }).then(response => this.setState({user: response}))}
  componentDidMount(){
    this.fetchData(this.props.date)
  
  }
  componentDidUpdate(prevProps){
    if(this.props.date!==prevProps.date) {
      this.fetchData(this.props.date)
    }

  }

  render() {
    return <div>
      
      
        {this.state.user.map((User) => <div>{User.username}</div>)}
        {this.state.user.map((User) => <div>{User.email}</div>)}
        {this.state.user.map((User) => <div>{User.userpic}</div>)}
        {this.state.user.map((User) => <div>{User.sex}</div>)}
        {this.state.user.map((User) => <div>{User.age}</div>)}
        
    </div>
  }
}

export default Uuser;