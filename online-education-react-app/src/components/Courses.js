import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class Ccourses extends React.Component {

  constructor(props) {
    super(props);
    this.state = { courses: [] };

  }

  fetchData = date => {api.courses({ page: date }).then(response => this.setState({courses: response}))}
  componentDidMount(){
    this.fetchData(this.props.date)
  
  }
  componentDidUpdate(prevProps){
    if(this.props.date!==prevProps.date) {
      this.fetchData(this.props.date)
    }

  }

  render() {
    console.log(this.props.date)
    return <div>

        {this.state.courses.map((Course) => <div>{Course.nameOfSubject} {Course.price} {Course.nameOfTeacher}</div>)}
    
    </div>
    
  }
}

export default Ccourses;