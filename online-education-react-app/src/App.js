import logo from './logo.svg';
import './App.css';
import Ccourses from "./components/Courses";
import Uuser from './components/User';
import Nnews from './components/News';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Online-Educational Univesity</h1>
        <p>
          <Router>
            <header class="header">
              <Link class="header__heading" to="/">
                Our online-educational service
              </Link>
            </header>
            <div className="App" class="app">
              <Switch>
                <Route path="/courses">
                    <Link to="/courses">
                      <button class="main-button">See list of courses</button>
                    </Link>
                    <Link to="/user">
                      <button class="main-button">See personal data</button>
                    </Link>
                    <Link to="/news">
                      <button class="main-button">See last news</button>
                    </Link>
                  <Ccourses />
                  <Ccourses />
                  <Ccourses />
                </Route>
                <Route path="/user">
                    <Link to="/courses">
                      <button class="main-button">See list of courses</button>
                    </Link>
                    <Link to="/user">
                      <button class="main-button">See personal data</button>
                    </Link>
                    <Link to="/news">
                      <button class="main-button">See last news</button>
                    </Link>
                  <Uuser />
                </Route>
                <Route path="/news">
                    <Link to="/courses">
                      <button class="main-button">See list of courses</button>
                    </Link>
                    <Link to="/user">
                      <button class="main-button">See personal data</button>
                    </Link>
                    <Link to="/news">
                      <button class="main-button">See last news</button>
                    </Link>
                  <Nnews />
                  <Nnews />
                  <Nnews />
                </Route>
                <Route path="/">
                  <main class="main">
                    <Link to="/courses">
                      <button class="main-button">See list of courses</button>
                    </Link>
                    <Link to="/user">
                      <button class="main-button">See personal data</button>
                    </Link>
                    <Link to="/news">
                      <button class="main-button">See last news</button>
                    </Link>
                    {""}
                      <h2 class="main-heading">New view at education science</h2>
                  </main>
                </Route>
              </Switch>
            </div>
            <footer class="footer">
              <div class="footer__copyright">© Online Education Univercity</div>
            </footer>
          </Router>
        </p> 
      </header>
    </div>
  );
}

export default App;
