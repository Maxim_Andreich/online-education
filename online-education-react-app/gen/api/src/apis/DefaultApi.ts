/* tslint:disable */
/* eslint-disable */
/**
 * Online-education-service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import {
    Course,
    CourseFromJSON,
    CourseToJSON,
    ErrorResponse,
    ErrorResponseFromJSON,
    ErrorResponseToJSON,
    News,
    NewsFromJSON,
    NewsToJSON,
    User,
    UserFromJSON,
    UserToJSON,
} from '../models';

export interface CoursesRequest {
    course?: string;
}

export interface NewsRequest {
    date?: string;
}

export interface UserRequest {
    date?: string;
}

/**
 * 
 */
export class DefaultApi extends runtime.BaseAPI {

    /**
     * Returns list of courses
     */
    async coursesRaw(requestParameters: CoursesRequest): Promise<runtime.ApiResponse<Array<Course>>> {
        const queryParameters: any = {};

        if (requestParameters.course !== undefined) {
            queryParameters['course'] = requestParameters.course;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/courses`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(CourseFromJSON));
    }

    /**
     * Returns list of courses
     */
    async courses(requestParameters: CoursesRequest): Promise<Array<Course>> {
        const response = await this.coursesRaw(requestParameters);
        return await response.value();
    }

    /**
     * Returns news
     */
    async newsRaw(requestParameters: NewsRequest): Promise<runtime.ApiResponse<Array<News>>> {
        const queryParameters: any = {};

        if (requestParameters.date !== undefined) {
            queryParameters['date'] = requestParameters.date;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/news`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(NewsFromJSON));
    }

    /**
     * Returns news
     */
    async news(requestParameters: NewsRequest): Promise<Array<News>> {
        const response = await this.newsRaw(requestParameters);
        return await response.value();
    }

    /**
     * Returns user profile
     */
    async userRaw(requestParameters: UserRequest): Promise<runtime.ApiResponse<Array<User>>> {
        const queryParameters: any = {};

        if (requestParameters.date !== undefined) {
            queryParameters['date'] = requestParameters.date;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/user`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(UserFromJSON));
    }

    /**
     * Returns user profile
     */
    async user(requestParameters: UserRequest): Promise<Array<User>> {
        const response = await this.userRaw(requestParameters);
        return await response.value();
    }

}
