/* tslint:disable */
/* eslint-disable */
/**
 * Online-education-service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface User
 */
export interface User {
    /**
     * 
     * @type {string}
     * @memberof User
     */
    username: string;
    /**
     * 
     * @type {string}
     * @memberof User
     */
    email?: string;
    /**
     * 
     * @type {string}
     * @memberof User
     */
    userpic?: string;
    /**
     * 
     * @type {string}
     * @memberof User
     */
    sex?: string;
    /**
     * 
     * @type {string}
     * @memberof User
     */
    age?: string;
}

export function UserFromJSON(json: any): User {
    return UserFromJSONTyped(json, false);
}

export function UserFromJSONTyped(json: any, ignoreDiscriminator: boolean): User {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'username': json['username'],
        'email': !exists(json, 'email') ? undefined : json['email'],
        'userpic': !exists(json, 'userpic') ? undefined : json['userpic'],
        'sex': !exists(json, 'sex') ? undefined : json['sex'],
        'age': !exists(json, 'age') ? undefined : json['age'],
    };
}

export function UserToJSON(value?: User | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'username': value.username,
        'email': value.email,
        'userpic': value.userpic,
        'sex': value.sex,
        'age': value.age,
    };
}


